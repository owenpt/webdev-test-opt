<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="scrViewport" name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Owen's dev test page for thehideout.co.uk">
    <meta name="author" content="Owen Prickett Treacy">
    <link rel="icon" href="themes/devtest/images/icons/opt-icon.png">

    <title>Owen's dev test page for thehideout.co.uk</title>
	
    <!-- Core CSS -->
    <link href="themes/devtest/scss/main.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/font-awesome.css" rel="stylesheet">

  </head>
  
  
<!-- BODY -->
<body>
	
<!-- core page container -->
<div class="container core-page"> 
	
	<h1>the hideout. dev-test</h1>
	<h2>Owen Prickett Treacy</h2>
	<img src="themes/devtest/images/icons/opt-icon.png" alt="a black and yellow logo, OPT">
	
	</br>
	</br>
	
	<!-- task 1 -->
	<div class="container task-title">
	  <div class="col-xs-3">
	    <img src="themes/devtest/images/tasks/task-1.png" alt="task one">
	  </div>
	  <div class="col-xs-9">
	    <h2>Simple cards</h2>
	  </div>
	</div>
	<!-- /task 1 -->
	  
	  
	<!-- task 1, tiles -->
	<div class="card-deck-wrapper">
	  <div class="card-deck">
		<div class="card">
		  <img class="card-img-top" src="themes/devtest/images/tiles/forest-mountain.jpg" alt="snowy, treetopped mountainside">
		  <div class="card-block">
			<p class="card-text">Ad pretium. Etiam parturient amet justo leo vestibulum, diam potenti mus praesent, sit suspendisse porta.</p>
		  </div>
		</div>
		<div class="card">
		  <img class="card-img-top" src="themes/devtest/images/tiles/mountain2.jpg" alt="a forest preceeding a distand, cloud covered mountain">
		  <div class="card-block">
			<p class="card-text">Ad pretium. Etiam parturient amet justo leo vestibulum, diam potenti mus praesent, sit suspendisse porta.</p>
		  </div>
		</div>
		<div class="card">
		  <img class="card-img-top" src="themes/devtest/images/tiles/waterfall.jpg" alt="a vibrant, blue waterfall">
		  <div class="card-block">
			<p class="card-text">Vehicula imperdiet tincidunt ultricies, id elit lacus, augue donec ante litora dapibus elementum sem volutpat litora vestibulum turpis proin. This box has more text and the others match its size.</p>
		  </div>
		</div>
	  </div>
	</div>
	<!--/task 1, tiles -->
	
	
	<!-- task 2 -->
	<div class="container task-title">
	  <div class="col-xs-3">
	    <img src="themes/devtest/images/tasks/task-2.png" alt="task two">
	  </div>
	  <div class="col-xs-9">
	    <h2>Expandable search feature</h2>
		<div id="sb-search" class="sb-search">
			<form>
				<input class="sb-search-input" placeholder="Enter a search..." type="search" value="" name="search" id="search">
				<input class="sb-search-submit" type="submit" value="">
				<span class="sb-icon-search"></span>
			</form>
		</div>
	  </div>
	</div>
	<!-- /task 2 -->
	
	
	<!-- task 3 -->
	<div class="container task-title">
	  <div class="col-xs-3">
	    <img src="themes/devtest/images/tasks/task-3.png" alt="task three">
	  </div>
	  <div class="col-xs-9">
	    <h2>Basic image gallery</h2>
	  </div>
	</div>
	  
	  <div class="container gallery-container">
		<div class="col-xs-2">
		  <img class="gallery-img" src="themes/devtest/images/tiles/coffee.jpg" alt="coffee cups on a wooden table">
		</div>
		<div class="col-xs-6">
		  <img class="gallery-img" src="themes/devtest/images/tiles/woods.jpg" alt="a small road meandering down a hillside forest">
		</div>
		<div class="col-xs-4">
		  <img class="gallery-img gallery-img-end" src="themes/devtest/images/tiles/red-leaves.jpg" alt="a stack of leaves, ranging through the autumn colours">
		</div>
		<div class="col-xs-4">
		  <img class="gallery-img gallery-img-end" src="themes/devtest/images/tiles/train-tracks.jpg" alt="a train junction, taken from close to the floor">
		</div>
		<div class="col-xs-6 gallery-bottom">
		  <img class="gallery-img" src="themes/devtest/images/tiles/night-sky.jpg" alt="a hazy night sky above a mountainline sillhouette">
		</div>
		<div class="col-xs-2 gallery-bottom">
		  <img class="gallery-img" src="themes/devtest/images/tiles/bench.jpg" alt="a couple observing the view from a bench">
		</div>
	  </div>
	<!-- /task 3 -->
	
	
	<!-- task 4 -->
	<div class="container task-title">
	  <div class="col-xs-3">
	    <img src="themes/devtest/images/tasks/task-4.png" alt="task four">
	  </div>
	  <div class="col-xs-9">
	    <h2>Single HTML file of this page</h2>
		<a href="https://bitbucket.org/owenpt/webdev-test-opt" title="Owen Prickett Treacy's Web Dev Test">https://bitbucket.org/owenpt/webdev-test-opt</a>
	  </div>
	</div>
	<!-- /task 4 -->

	<!-- FOOTER -->
	<hr class="featurette-divider">
	  
	  <div class="container">
	  <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; <?php echo date("Y"); ?> Owen Prickett Treacy &middot; 
      </footer>
	  </div>
	  
<!-- /core page container -->  
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

<!-- Search Box JS -->
<script src="themes/devtest/js/searchbox/classie.js"></script>
<script src="themes/devtest/js/searchbox/uisearch.js"></script>
<script>
	new UISearch( document.getElementById( 'sb-search' ) );
</script>
	
  </body>
</html>
